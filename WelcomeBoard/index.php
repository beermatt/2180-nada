<?php session_start(); ?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="ford-fonts/ford-fonts.css">
<script src="jquery/jquery-3.1.1.min.js"></script>
<style>
*, *:before, *:after {
	box-sizing: inherit;
	margin: 0;
	padding: 0;
}

html {
	width: 100%;
	height: 100%;
	box-sizing: border-box;

}
</style>
<script>
$(document).ready(function(){
	var name_list='';

	setInterval(function(){
		$.ajax({
        		url: 'get_names.php',
        		type: 'POST'
    		}).done(function(new_names_list) {
			if (name_list != new_names_list) {
				$('#namecontainer').html(new_names_list);
				name_list = new_names_list;
			}
		});
        },3000);
});
</script>
</head>
<!-- <body style="background-image:url(images/home-background.jpg); background-repeat: no-repeat; overflow:hidden; background-size: 100% 100%; width: 100%; height: 100%;"> -->
<body style="background-image:url(images/fcsd_back-temp.jpg); background-repeat: no-repeat; overflow:hidden; background-size: 100% 100%; width: 100%; height: 100%;">

<div id="namecontainer" style="width: 750px; height: 630px; margin: 50px 50px 0px 0px; float:right;">

<?php

///get names from cloud
// $get_names = file_get_contents('http://192.168.0.11/getnames.php');
$get_names = file_get_contents('http://localhost/2180-nada/WelcomeBoard/getnames.php');
$get_names_array = json_decode($get_names, true);
$_SESSION['current_names']=$get_names_array;
$html = '';

foreach($get_names_array as $k => $v){

	// if($k > 0){

			$html = '<div style="width: 100%; height: 40px; margin-bottom: 10px; background-color: #fff;"><div style="width: 25px; height: 25px; margin: 7px 0px 0px 20px; position:absolute; background-image:url(images/people-icon.png); background-repeat: no-repeat;"></div><div style="width: 225px; height: 35px; margin: 3px 0px 0px 80px; position:absolute; font-size:16pt; font-family: FordAntenna Medium; color: #092e6e;">'.ucwords(strtolower($v['name'])).'</div><div style="width: 420px; height: 35px; margin: 3px 0px 0px 325px; position:absolute; font-size:16pt; font-family: FordAntenna Medium; color: #092e6e;">'.ucwords(strtolower($v['dealer'])).'</div></div>'.$html;

	// }

}

echo $html;
// echo 'asdf1';

?>

</div>

</body>
</html>
