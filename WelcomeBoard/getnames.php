<?php // NOTE: From files Mazin found
// error_reporting(0); // Prod (No errors reported)
error_reporting(-1);
// $host = "xperience-us.com";
// $db_name = "xperdev_nada";
// $db_user = "xperdev";
// $db_pass = "";
$host = "localhost";
$db_name = "nada_2017";
$db_user = "root";
$db_pass = "";

// @$mysqli = new mysqli($host,$db_user,$db_pass,$db_name); // NOTE: Ori/ Pos prod
$mysqli = new mysqli($host,$db_user,$db_pass,$db_name); // NOTE: FOR TESTING
if (!$mysqli) {
   $retval = array('ERROR'=>'Could not connect to database');
   print json_encode($retval);
   exit;
}

$query = "select concat(FirstName,' ',LastName) as name, DealerName as dealer from registrations order by time_in desc limit 8";
$result = $mysqli->query($query);
if (!$result) {
   $retval = array('ERROR'=>'Could not query registrations');
   print json_encode($retval);
   $mysqli->close();
   exit;
}
$names = array();
while ($names[]=$result->fetch_assoc());
array_pop($names);
print json_encode($names);
$mysqli->close();
exit;
