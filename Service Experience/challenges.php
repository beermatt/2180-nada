<?php session_start(); ?>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="style-challenges.css">
</head>
<body>
<div id="Content">

	<?php include('titlebar.php'); ?>
    
    <div id="mainContent" style="height: 100%; width: 75%; margin: 300px auto; text-align:center;">
    
    	<?php if(in_array('1', $_SESSION['nadasape']['challenges'])){ ?>
        
        <div id="challOption"><div id="checkMarkOn"></div><div class="OptionTitles">Online<br>Search<br>Experience</div></div>
        
        <?php } else { ?>
        
        <a href="online-search.php"><div id="challOption"><div id="checkMark"></div><div class="OptionTitles">Online<br>Search<br>Experience</div></div></a>
        
        <?php } ?>
        
    	
		
		<?php if(in_array('2', $_SESSION['nadasape']['challenges'])){ ?>
        
        <div id="challOption"><div id="checkMarkOn"></div><div class="OptionTitles">Reputation<br>Management<br>Experience</div></div>
        
        <?php } else { ?>
        
        <a href="reputation-management.php"><div id="challOption"><div id="checkMark"></div><div class="OptionTitles">Reputation<br>Management<br>Experience</div></div></a>
        
        <?php } ?>
        
    	
		
		<?php if(in_array('3', $_SESSION['nadasape']['challenges'])){ ?>
        
        <div id="challOptionR"><div id="checkMarkOn"></div><div class="OptionTitles">Website<br>Assessment<br>Experience</div></div>
        
        <?php } else { ?>
        
        <a href="website-assessment.php"><div id="challOptionR"><div id="checkMark"></div><div class="OptionTitles">Website<br>Assessment<br>Experience</div></div></a>
        
        <?php } ?>                
    
    
    </div>
    
</div>

</body>
</html>