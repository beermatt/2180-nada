<?php

$host = 'localhost';
$db_name = 'nada_2017';
$db_user = 'root';
$db_pass = '';
$mysqli = new mysqli($host, $db_user, $db_pass);

$query = "CREATE DATABASE IF NOT EXISTS {$db_name}"; if(!$mysqli->query($query)){ echo "$query"; }

$mysqli->select_db($db_name);

$query = "CREATE TABLE IF NOT EXISTS `link_dealers_spe_challenges` (
  `challengeID` int(11) NOT NULL,
  `dealerID` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;";

if(!$mysqli->query($query)){ echo "$query"; }


$query = "CREATE TABLE IF NOT EXISTS `challenge_results` (
  `resultsID` int(11) NOT NULL AUTO_INCREMENT,
  `dealerRFID` varchar(50) DEFAULT NULL,
  `dealerID` int(11) DEFAULT NULL,
  `challenge_type` text DEFAULT NULL,
  `results_data` text DEFAULT NULL,
  `date_time` timestamp DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`resultsID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;";

if(!$mysqli->query($query)){ echo "$query"; }



?>