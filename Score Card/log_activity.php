<?php
// log activity
// write to database from post
// 2017-01-09 mhowell
// 2017-01-11 mhowell updated to correct sent field and add index
// call this via ajax with post parms of data (name of activity) and rfid (the rfid/qr code of the attendee)

$local_db = 'nada';
$local_table = 'activities';

foreach ($_GET as $n=>$v) {
   $_POST[$n]=$v;
}

$db = new mysqli('localhost','root','');

$create_query = "CREATE DATABASE IF NOT EXISTS `{$local_db}`";
$result = $db->query($create_query);

$db->select_db($local_db);

$create_query = "CREATE TABLE IF NOT EXISTS `{$local_table}` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `rfid` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `time_in` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sent` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=1 ";
$result = $db->query($create_query);

$create_query = "ALTER TABLE `{$local_table}` ADD UNIQUE `data-time` (`data`, `time_in`)";
$result = $db->query($create_query);

if (isset($_POST['data'])) {
	if (!isset($_POST['rfid'])) {
		$_POST['rfid']='';
	}
	$query = "insert into {$local_table} (data,rfid) VALUES ('".$_POST['data']."','".$_POST['rfid']."')";
	$result = $db->query($query);

}

$query = "select * from {$local_table} where sent=0";
$result = $db->query($query);
$rows = array();
while ($rows[]=$result->fetch_assoc());
array_pop($rows);
foreach ($rows as $one_row) {
	$url = 'http://192.168.0.11/sendactivity.php?data='.urlencode($one_row['data']).'&time_in='.urlencode($one_row['time_in']).'&rfid='.urlencode($one_row['rfid']);
	$response = file_get_contents($url);
	if ($response == '1') {
		$query = "update {$local_table} set sent=1 where id=".$one_row['id'];
                $result = $db->query($query);
	}
       
}