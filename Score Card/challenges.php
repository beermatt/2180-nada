<?php session_start(); ?>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="style-challenges.css">
<script src="jquery/jquery-2.2.3.min.js"></script>
<script>
$(document).ready(function() {
$.ajax({
        url: 'log_activity.php',
        type: 'POST',
        dataType: "json",
        data: {
            data: "Parts Experience - Scorecard",
	    rfid: "<?php echo $_SESSION['rfid']; ?>"
        }
    });
});
</script>
</head>
<body>
<div id="Content">

	<?php include('titlebar.php'); ?>
    
    <div style="height: 50px; width: 1200px; margin-top: 134px; margin-left: 300px; font-family: fordantenna light; color: #fff; font-size: 17pt; padding: 10px; position:absolute;"><?php echo strtoupper($_SESSION['nadasape']['data']['DealerName']); ?></div>
    
    <div style="height: 50px; width: 300px; margin-top: 230px; margin-left: 520px; font-family: fordantenna light; color: #fff; font-size: 17pt; padding: 10px; position:absolute;"><?php echo strtoupper($_SESSION['scorecarddata']['data']['Enrolled']); ?></div>
    
    <div style="height: 50px; width: 150px; margin-top: 230px; margin-left: 1150px; font-family: fordantenna light; color: #fff; font-size: 16pt; padding: 10px; position:absolute;"><?php echo strtoupper($_SESSION['scorecarddata']['data']['2014_Rank']); ?></div>
    
    <div style="height: 50px; width: 150px; margin-top: 230px; margin-left: 1600px; font-family: fordantenna light; color: #fff; font-size: 16pt; padding: 10px; position:absolute;"><?php echo strtoupper($_SESSION['scorecarddata']['data']['2015_Rank']); ?></div>
    
    <div style="height: 50px; width: 600px; margin-top: 354px; margin-left: 290px; font-family: fordantenna light; color: #fff; font-size: 17pt; padding: 10px; position:absolute;">$<?php echo number_format($_SESSION['scorecarddata']['data']['Total_Sales'], 2); ?></div>
    
    <div style="height: 50px; width: 300px; margin-top: 354px; margin-left: 1528px; font-family: fordantenna light; color: #fff; font-size: 17pt; padding: 10px; position:absolute;">$<?php echo number_format($_SESSION['scorecarddata']['data']['Total_Sales_Average'], 2); ?></div>
    
    <div style="height: 50px; width: 600px; margin-top: 448px; margin-left: 290px; font-family: fordantenna light; color: #fff; font-size: 17pt; padding: 10px; position:absolute;"><?php echo number_format($_SESSION['scorecarddata']['data']['B2B_Sales_Percent'], 2); ?>%</div>
    
    <div style="height: 50px; width: 300px; margin-top: 448px; margin-left: 1528px; font-family: fordantenna light; color: #fff; font-size: 17pt; padding: 10px; position:absolute;"><?php echo number_format($_SESSION['scorecarddata']['data']['B2B_Sales_Percent_Average'], 2); ?>%</div>
    
    <div style="height: 50px; width: 600px; margin-top: 544px; margin-left: 290px; font-family: fordantenna light; color: #fff; font-size: 17pt; padding: 10px; position:absolute;"><?php echo number_format($_SESSION['scorecarddata']['data']['B2C_Sales_Percent'], 2); ?>%</div>
    
    <div style="height: 50px; width: 300px; margin-top: 544px; margin-left: 1528px; font-family: fordantenna light; color: #fff; font-size: 17pt; padding: 10px; position:absolute;"><?php echo number_format($_SESSION['scorecarddata']['data']['B2C_Sales_Percent_Average'], 2); ?>%</div>
   
</div>

</body>
</html>