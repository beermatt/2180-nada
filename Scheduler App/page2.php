<?php
	$devFolderName = '~git'; //
	define('PRODUCTION', !stristr($_SERVER['SCRIPT_NAME'], $devFolderName));

	$user = json_decode($_GET['data'], true);
	$user['DealerPNA'] = str_pad($user['DealerPNA'], 5, 0, STR_PAD_LEFT);
// var_dump($user);
?>

<html>
<head>
	<title>Ford Parts Appointment Scheduling Tablet</title>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

	<link rel="stylesheet" href=".\styleSchedule.css" />
	<link rel="stylesheet" href=".\ford-fonts\ford-fonts.css" />

	<script type='text/javascript' src='./jquery-3.1.1.min.js'></script>

	<script type="text/javascript">
		$(document).ready(function(){

			$("#Add_Name_First").focus();

			var PRODUCTION = <?php echo json_encode(PRODUCTION); ?>;
			// var MY_SERVER = "http://192.168.0.11/";
			var MY_SERVER = "http://localhost/";
			console.log(MY_SERVER);

			$('#Finished').click(function(){
				console.log("Finished clicked");

				$.ajax({
					type: "GET",
					url: "./storelocal.php",
					data: {
						Tracker			: "<?php echo $user['DealerPNA']; ?>",
						Add_Name_First	: $('#Add_Name_First').val(),
						Add_Name_Last	: $('#Add_Name_Last').val(),
						Add_Email		: $('#Add_Email').val(),
						Add_Phone		: $('#Add_Phone').val(),
						Add_Notes		: $('#Add_Notes').val()
					},
				}).done(function(msg) {
					console.log(msg);
					//window.location.href = msg[2] ?  "./page2.php?data=" + msg.slice(1, -1) : "./index.php";
				});

				$.ajax({
					type: "GET",
					url: MY_SERVER + "2180-nada/sendappt.php?Tracker=" + "<?php echo $user['DealerPNA']; ?>"
					+ "&Extra_FirstName=" + $('#Add_Name_First').val()
					+ "&Extra_LastName=" + $('#Add_Name_Last').val()
					+ "&Extra_Email=" + $('#Add_Email').val()
					+ "&Extra_Phone=" + $('#Add_Phone').val()
					+ "&Extra_Notes=" + $('#Add_Notes').val()
					+ "",
				}).done(function(msg) {
					console.log(msg);
					window.location.href = msg[0] ?  "./page3.php" : window.location.href;
				});

			});

			$('#Clear').click(function(){
				console.log("Clear clicked");
				// $('#FormMid').find('input').val("");
				// $('#Add_Notes').val("");
				window.location.href = "./index.php";
			});

			// $('#Home').click(function(){
			// 	console.log("Home clicked");
			// 	window.location.href = "./index.php";
			// });
		});
	</script>
</head>

<body >
<!-- <a href="./index.php"></a> -->
<div>

</div>

<div id="ScheduleWrapper" style="">

<div id="PageHeader">
	<b>WELCOME</b> <?php $headertext = $user['FirstName'] . " " . $user['LastName'] . ", " . $user['DealerName'] . " - " . str_pad($user['DealerPNA'], 5, 0, STR_PAD_LEFT) ; echo strtoupper($headertext) ?>
	<div id="Clear"></div>
	<!-- <div id="Home"></div> -->

</div>

<div id="FormWrapper">
	<div id="FormTop" class="flex-form">
		<div class="form3">FIRST NAME:	<input disabled id="Name_First" 	value="<?php echo $user['FirstName']; ?>"></input> </div>
		<div class="form3">LAST NAME:	<input disabled id="Name_Last" 	value="<?php echo $user['LastName']; ?>"></input> </div>
		<div class="form3">DEALER NAME:	<input disabled id="Name_Dealer" value="<?php echo $user['DealerName']; ?>"></input> </div>
		<div class="form3">CITY:		<input disabled id="City" 		value="<?php echo $user['DealerCity']; ?>"></input> </div>
		<div class="form3">EMAIL:		<input disabled id="Email" 		value="<?php echo $user['EmailAddress']; ?>"></input> </div>
		<div class="form3">PHONE #:		<input disabled id="Phone" 		value="<?php echo $user['CellPhone']; ?>"></input> </div>
	</div>
	<div id="FormMid" class="flex-form">
		<div class="form3">FIRST NAME:	<input id="Add_Name_First" 		value=""></input></div>
		<div class="form3">LAST NAME:	<input id="Add_Name_Last" 		value=""></input></div>
		<div class="form3">EMAIL:		<input id="Add_Email" 			value=""></input></div>
		<div class="form3">PHONE #:		<input id="Add_Phone" 			value=""></input></div>
		<div class="formM"><span>NOTES:</span><textarea id="Add_Notes"	value=""></textarea></div>
	</div>
	<div if="FormLow">
		<div id="Finished"></div>
	</div>
</div>
</div>
