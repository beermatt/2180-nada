<?php
	$devFolderName = '~git'; //
	define('PRODUCTION', !stristr($_SERVER['SCRIPT_NAME'], $devFolderName));

?>

<html>
<head>
	<title>Ford Parts Appointment Scheduling Tablet</title>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

	<script type='text/javascript' src='./jquery-3.1.1.min.js'></script>
			
	<script type="text/javascript">
		$(document).ready(function(){
			// #myjavascriptisfullofeels
			var PRODUCTION = <?php echo json_encode(PRODUCTION); ?>;
			var MY_SERVER = "http://192.168.0.11/";

			setInterval(function() {
				window.location.href = "./index.php";
			}, 3000);
			
		});
	</script>
	
	<link rel="stylesheet" href=".\ford-fonts\ford-fonts.css" />
	<style type="text/css">
		*, *:before, *:after {
		  box-sizing: border-box;
		}
		
		body: {
			margin: 0px;
		}

		#ThanksWrapper {
			width: 100%;
			height: 100%;
			background: url('./images/thanks-bg.jpg');
			background-size: cover;
			background-position: 50% 50%;
			background-repeat: no-repeat;
			overflow: hidden;
		}

		div#Message {
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translateX(-50%) translateY(-50%);
			/*outline: 1px solid turquoise;*/
			height: 13vh;
			line-height: 13vh;
			width: 60vw;
			text-align: center;
			color: white;
			font-size: 4em;
			font-family: 'FordAntenna Light';
		}

		div#Message b {
			font-size: 3em;
			font-family: 'FordAntenna Bold';
		}		
	</style>
</head>

<body style="overflow: hidden; margin:0px;">
<!-- <a href="./index.php"></a> -->

<div id="ThanksWrapper" style="position: absolute;">
	<div id="Message">
		<b>Thank You!</b>
	</div>

</div>
