<?php
	$devFolderName = '~git'; //
	define('PRODUCTION', !stristr($_SERVER['SCRIPT_NAME'], $devFolderName));

?>

<html>
<head>
	<title>Ford Parts Appointment Scheduling Tablet</title>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

	<link rel="stylesheet" href="styleScan.css" />

	<script type='text/javascript' src='./jquery-3.1.1.min.js'></script>

	<script type="text/javascript">
		$(document).ready(function(){
			// #myjavascriptisfullofeels
			var hasScan = 0;
			var PRODUCTION = <?php echo json_encode(PRODUCTION); ?>;
			// var MY_SERVER = "http://192.168.0.11/";
			var MY_SERVER = "http://localhost/";

			//PRODUCTION && (console.log = function () {});
			console.log(MY_SERVER);

			setInterval(function() {
				if (!$("#RFIDinput").is(':focus')) {
					console.log("Setting focus to #RFIDinput");
					$('#RFIDinput').focus();
				}
			}, 500);

			$('#RFIDinput').keypress(function(event){
				var data = {};

				if(event.type == 'keypress' && event.which == '13'){
					var tracker = $('#RFIDinput').val();

					$.ajax({
						type: "GET",
						url: MY_SERVER + "2180-nada/getnamebyrfid.php?rfid=" + tracker,
					}).done(function(msg) {
						console.log(msg);
						alert(msg);
						window.location.href = msg[2] ?  "./page2.php?data=" + msg.slice(1, -1) : "./index.php";
						//msg = JSON.parse(msg); //not needed
					});
				}
			});	//end ('#RFIDinput').keypress

		});
	</script>
</head>

<body style="overflow: hidden; margin:0px;">
<!-- <a href="./index.php"></a> -->
<div>
<input type="text" id="RFIDinput" style="position: absolute;" />
</div>

<div id="ScanWrapper" style="position: absolute;">


</div>
