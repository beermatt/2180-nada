<?php

error_reporting(1);
@ini_set('display_errors', 1);

$host = 'localhost';
$db_name = 'nada_2017';
$db_table = 'pna_codes';
$db_user = 'root';
$db_pass = '';


$mysqli = new mysqli($host, $db_user, $db_pass, $db_name);

if (!isset($_POST['state'])) { echo "State not set"; die; } else { $state = $_POST['state']; }

$query = "SELECT DISTINCT `Dealer_City` FROM `{$db_table}` WHERE `Dealer_State` = '{$state}' ORDER BY `Dealer_City` ASC;";
$results = $mysqli->query($query);

echo '<option value="" disabled selected>DEALERSHIP CITY</option>';

while($singleresult = $results->fetch_assoc()){
	if ($singleresult['Dealer_City'] != '') { 
		$curstate =  $singleresult['Dealer_City'] ;
		//echo $curstate . "1 -> " . $curstate . "2 -> " . $curstate . "3 -> " . $curstate . "4 -> " . $curstate . "5 -> <br/>" ;
		echo '<option value="'. $curstate .'">'. $curstate .'</option>';
	}
}

?>