<?php

error_reporting(1);
@ini_set('display_errors', 1);

$host = 'localhost';
$db_name = 'nada_2017';
$db_table = 'pna_codes';
$db_user = 'root';
$db_pass = '';


$mysqli = new mysqli($host, $db_user, $db_pass, $db_name);

$query = "SELECT DISTINCT `Dealer_State` FROM `{$db_table}` ORDER BY `Dealer_State` ASC;";

$results = $mysqli->query($query);

echo '<option value="" disabled selected>DEALERSHIP STATE</option>';

while($singleresult = $results->fetch_assoc()){

	if ($singleresult['Dealer_State'] != '') { 

		$curstate =  $singleresult['Dealer_State'] ;

		echo '<option value="'. $curstate .'">'. ucwords(strtolower($curstate)).'</option>';
	}
}

?>