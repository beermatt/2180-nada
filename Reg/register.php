<?php

session_start();

include('config.php');
include('cloud_push.php');

////step 1......save form data pre-scan into session
if($_POST['step'] == 1){
	
	////start clean
	unset($_SESSION['nadareg']);
	
	///set new data
	$_SESSION['nadareg'] = $_POST;
	
	///redirect to scan rfid page	
	header("Location: scan.php");
	
}


////step 2......get scan rfid code, match pna code from dealership, stire locally in DB
if($_POST['step'] == 2){
	
	///get pna code
	$q = $mysqli->query("SELECT PNA_Code FROM pna_codes WHERE Dealer_Name = \"".$_SESSION['nadareg']['DealerName']."\"");
	$r = $q->fetch_assoc();
	
	///text optin switch
	if($_SESSION['nadareg']['TextOptin'] == 'No'){ $TexdOptin = 'No'; } else { $TexdOptin = 'Yes';  }
	
	///insert into local DB
	$mysqli->query("insert INTO registrations(FirstName, LastName, EmailAddress, CellPhone, TextOptin, DealerState, DealerCity, DealerName, DealerPNA, DealerRFID) VALUES ('".addslashes($_SESSION['nadareg']['FirstName'])."', '".addslashes($_SESSION['nadareg']['LastName'])."', '".addslashes($_SESSION['nadareg']['EmailAddress'])."', '".addslashes($_SESSION['nadareg']['CellPhone'])."', '".$TexdOptin."', '".addslashes($_SESSION['nadareg']['DealerState'])."', '".addslashes($_SESSION['nadareg']['DealerCity'])."', '".addslashes($_SESSION['nadareg']['DealerName'])."', '".$r['PNA_Code']."', '".$_POST['DealerRFID']."')");
	
	$newrecordID = $mysqli->insert_id;
	
	////add to cloud and mark sent if sucessful
	$parms = array('FirstName'=>''.$_SESSION['nadareg']['FirstName'].'','LastName'=>''.$_SESSION['nadareg']['LastName'].'','EmailAddress'=>''.$_SESSION['nadareg']['EmailAddress'].'','CellPhone'=>''.$_SESSION['nadareg']['CellPhone'].'', 'TextOptin'=>''.$TexdOptin.'', 'DealerState'=>''.$_SESSION['nadareg']['DealerState'].'', 'DealerCity'=>''.$_SESSION['nadareg']['DealerCity'].'', 'DealerName'=>''.addslashes($_SESSION['nadareg']['DealerName']).'', 'DealerPNA'=> ''.$r['PNA_Code'].'', 'DealerRFID'=>''.$_POST['DealerRFID'].'');
	
	$sendLeads = sendLead($parms);
	
	if($sendLeads == 1){ $mysqli->query("UPDATE registrations SET sent = 1 WHERE id = '".$newrecordID."'"); }
	
	////clear session
	unset($_SESSION['nadareg']);
	
	///redirect to home page
	header("Location: index.php");
	
}


?> 