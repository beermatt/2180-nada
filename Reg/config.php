<?php

$host = 'localhost';
$db_name = 'nada_2017';
$db_table = 'registrations';
$db_user = 'root';
$db_pass = '';
$mysqli = new mysqli($host, $db_user, $db_pass);

$query = "CREATE DATABASE IF NOT EXISTS {$db_name}"; if(!$mysqli->query($query)){ echo "$query"; }

$mysqli->select_db($db_name);

$query = "CREATE TABLE IF NOT EXISTS `registrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `EmailAddress` varchar(50) DEFAULT NULL,
  `CellPhone` varchar(50) DEFAULT NULL,
  `TextOptin` varchar(4) DEFAULT NULL,
  `DealerState` varchar(50) DEFAULT NULL,
  `DealerCity` varchar(50) DEFAULT NULL,
  `DealerName` varchar(50) DEFAULT NULL,
  `DealerPNA` varchar(16) DEFAULT NULL,
  `DealerRFID` varchar(50) DEFAULT NULL,
  `time_in` datetime DEFAULT CURRENT_TIMESTAMP,
  `time_updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `got_text` int(1) NOT NULL DEFAULT '0',
  `sent` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;";

if(!$mysqli->query($query)){ echo "$query"; } // NOTE: Not recommended, should be displaying a "Out Of Order" page, not the failed query.

?>
