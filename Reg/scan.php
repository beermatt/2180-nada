<?php session_start(); ?>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="style.css">
<script src="jquery/jquery-2.2.3.min.js"></script>
<script>
/////no backspace
$(document).on("keydown", function (event) { 
  if (event.which === 8 && !$(event.target).is("input, textarea")) { 
   event.preventDefault(); 
  } 
});

$(document).on('touchstart click', function() {
    $("#DealerRFID").focus();
});
</script>

</head>
<body>
<div id="scan-wrapper">
  <div id="Content">
  	<div id="FormContainer" style="margin-top: 300px;">
      	<form action="register.php" method="post" name="scan" id="scan" autocomplete="off">
          <input type="hidden" name="step" value="2">
          <input type="input" name="DealerRFID" id="DealerRFID" value="" autofocus autocomplete="off" style="border:#fff solid 1px; position:absolute; bottom: 0; border-collapse: collapse; color: #fff; outline:0px; opacity: 0;">
  		</form>

  	</div>
  </div>
</div>

<script>
$(window).load(function () {
    window.setTimeout(function () {
        window.location.href = "reset-reg.php";
    }, 60000)
});
</script>
</body>
</html>