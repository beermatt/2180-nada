<?php session_start(); include('config.php'); unset($_SESSION['nadareg']);?>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="style.css">
<script src="jquery/jquery-2.2.3.min.js"></script>
<script src="jquery/jquery.validate.js"></script>
<script src="jquery/jquery.maskedinput.js"></script>
<script>

/////ajax for populating dealership info
$(document).ready(function(){

	$('#FirstName').focus();

	$.ajax({
		type: "POST",
		url: "get_states.php",
	}).done(function(msg) {
		$('#DealerState').html(msg);
	});

	$('#DealerState').on('change', function() {
		$.ajax({
			type: "POST",
			url: "get_cities.php",
			data: {state: $('#DealerState').val()},
		}).done(function(msg) {
			$('#DealerCity').html(msg);
		});
	});

	$('#DealerCity').on('change', function() {
		$.ajax({
			type: "POST",
			url: "get_names.php",
			data: {city: $('#DealerCity').val()},
		}).done(function(msg) {
			$('#DealerName').html(msg);
		});
	});	

	$('#DealerName').on('change', function() {
		$.ajax({
			type: "POST",
			url: "get_pna.php",
			data: {name: $('#DealerName').val()},
		}).done(function(msg) {
			$('#DealerPNA').val(msg);
		});
	});
});


/////no backspace
$(document).on("keydown", function (event) { 
  if (event.which === 8 && !$(event.target).is("input, textarea")) { 
   event.preventDefault(); 
  } 
});


////field masking phone
jQuery(function($){ $("#CellPhone").mask("(999)999-9999", {autoclear: false}); });

</script>

</head>
<body>
<div id="home-wrapper">
	<div id="Content">
		<div id="FormContainer" >
	    	<form action="register.php" method="post" name="SignupForm" id="SignupForm" autocomplete="off">
	        	<input type="hidden" name="step" value="1">
				<input id="FirstName" name="FirstName" type="text" class="signuptext" placeholder="FIRST NAME" />
				<input id="LastName" name="LastName"  type="text" class="signuptext" placeholder="LAST NAME"  />
				<input id="EmailAddress" name="EmailAddress" type="text" class="signuptext" placeholder="EMAIL ADDRESS" />
				<input id="CellPhone" name="CellPhone" type="text" class="signuptext" placeholder="CELL PHONE" />
				<select id="DealerState" name="DealerState" class="signuptext" placeholder="DEALERSHIP STATE" /><option value="" selected>DEALERSHIP STATE</option></select>
				<select id="DealerCity" name="DealerCity"  class="signuptext" placeholder="DEALERSHIP CITY"  /><option value="" disabled selected>DEALERSHIP CITY</option></select>
				<select id="DealerName" name="DealerName"  class="signuptext" placeholder="DEALERSHIP NAME"  /><option value="" disabled selected>DEALERSHIP NAME</option></select>
				<button id="SubmitButton" type="button"><input type="submit" value="SUBMIT" style="width: 330px; height: 40px; background-color: #06A7E2; border: 0px; color: #fff; font-size:20pt;"></button>
				<div class="checkboxwrapper">
					<label class="centervertical"><input id="OptoutBox" name="TextOptin" type="checkbox" class="signupcheckbox" value="No"/>I do not want to recieve text messages</label>
				</div>		
			</form>

		</div>
	</div>
</div>

<script>
$("#SignupForm").validate({

onfocusout: false,
onkeyup: false,
onclick: false,

rules: {
		FirstName: { required: true },
		LastName: { required: true },
		EmailAddress: { email: true, required: false },
		DealerState: { required: true },
		DealerCity: { required: true },
		DealerName: { required: true },
		},

errorPlacement: function(){

return false;  // suppresses error message text

} 

});

$(window).load(function () {
    window.setTimeout(function () {
        window.location.href = "reset-reg.php";
    }, 300000)
});
</script>

</body>
</html>