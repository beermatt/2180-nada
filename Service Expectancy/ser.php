<?php

session_start();

error_reporting(0);

$shade = $_GET['shade'];

///next page
$np = $shade+1;

if($np > 15){ $np = 15; }

///prev page
$pp = $shade-1;

if($pp < 0){ $pp = 0; }

?>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="style-challenges.css">
</head>
<body style="background-image: url(images/start-challenges-off.jpg);">
<div id="Content">

	<?php include('titlebar2.php'); ?>
    
    <a href="ser.php?shade=<?php echo $pp; ?>"><div style="z-index: 2; width: 150px; height: 150px; position:absolute; left: 125px; top: 700px;"><img src="images/left-arrow.png"/></div></a>
    <a href="ser.php?shade=<?php echo $np; ?>"><div style="z-index: 2; width: 150px; height: 150px; position:absolute; right: 125px; top: 700px;"><img src="images/right-arrow.png"/></div></a>
    
    <div style="z-index: 0; position:absolute; width: 100%; min-height: 1620px; background-image:url(images/<?php echo $_SESSION['nadaserexp']['data']['DealerPNA']; ?>.jpeg); background-size: 100% 100%;"></div>
    
    <div style="z-index: 1; position:absolute; width: 100%; min-height: 1620px; background-image:url(shades/2105_FCSD_NADA_Service_Expectancy_Report_Screens_V2-<?php echo $_GET['shade']; ?>.png); background-size: 100% 100%;"></div>
   
</div>

</body>
</html>