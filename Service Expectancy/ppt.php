<?php

session_start();

error_reporting(0);

$img = $_GET['img'];

///next page
$np = $img+1;

if($np > 4){ $np = 4; }

///prev page
$pp = $img-1;

if($pp < 1){ $pp = 1; }

?>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="style-challenges.css">
</head>
<body style="background-image: url(images/start-challenges-off.jpg);">
<div id="Content">

	<?php include('titlebar.php'); ?>

    <div id="mainContent" style="height: 1200px; width: 90%; margin: 100px auto; background-color: #fff; background-image: url(images/<?php echo $_GET['img']; ?>.png);
	background-repeat: no-repeat; background-size: 100% 100%;"></div>
    
    <a href="ppt.php?img=<?php echo $pp; ?>"><div style="z-index: 2; width: 150px; height: 150px; position:fixed; left: 145px; bottom: 30px;"><img src="images/left-arrow.png" width="100px" height="100px"/></div></a>

    <a href="ppt.php?img=<?php echo $np; ?>"><div style="z-index: 2; width: 150px; height: 150px; position:fixed; left: 300px; bottom: 30px;"><img src="images/right-arrow.png" width="100px" height="100px"/></div></a>
   
</div>

</body>
</html>