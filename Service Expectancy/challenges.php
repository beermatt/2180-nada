<?php session_start(); error_reporting(0); ?>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="style-challenges.css">
<script src="jquery/jquery-2.2.3.min.js"></script>
<script>
$(document).ready(function() {
$.ajax({
        url: 'log_activity.php',
        type: 'POST',
        dataType: "json",
        data: {
            data: "Service Expectancy",
	    rfid: "<?php echo $_SESSION['rfid']; ?>"
        }
    });
});
</script>
</head>
<body>
<div id="Content">

	<?php include('titlebar.php'); ?>
    
    <div id="mainContent" style="height: 100%; width: 75%; margin: 400px auto; text-align:center;">
    
        <a href="ser.php?shade=0"><div id="challOption"><div class="OptionTitles">SERVICE<br>EXPECTANCY<br>REPORT</div></div></a>
        
        <a href="ppt.php?img=1"><div id="challOption"><div class="OptionTitles">SERVICE<br>EXCELLENCE<br>2.0</div></div></a>
        
    </div>
    
</div>

</body>
</html>